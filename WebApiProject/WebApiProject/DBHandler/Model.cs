﻿using MWRetailOnboarding.Swagger.BankSmart.CreateAccount;
using System.Collections.Generic;

namespace DBHandler
{
    public class Model
    {
        public class Dtos
        {
            public class CreateAccount
            {
                public List<string> ClientIDS { get; internal set; }
                public string SignatureInstructions { get; internal set; }
                internal Account Acc { get; set; }
            }
        }
    }
}