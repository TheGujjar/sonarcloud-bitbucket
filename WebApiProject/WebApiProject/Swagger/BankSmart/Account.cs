﻿namespace MWRetailOnboarding.Swagger.BankSmart.CreateAccount
{
    internal class Account
    {
        public Account()
        {
        }

        public string CreateBy { get; set; }
        public string OurBranchID { get; set; }
        public string ClientID { get; set; }
        public string ProductID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string CountryID { get; set; }
        public string StateID { get; set; }
        public string CityID { get; set; }
        public string StatementFrequency { get; set; }
        public int HoldMail { get; set; }
        public int ZakatExemption { get; set; }
        public string IntroducerAccountNo { get; set; }
        public string IntroducedBy { get; set; }
        public string IntroducerAddress { get; set; }
        public string IntroducerCityID { get; set; }
        public string IntroducerStateID { get; set; }
        public string IntroducerCountryID { get; set; }
        public string ModeOfOperation { get; set; }
        public string Reminder { get; set; }
        public string Notes { get; set; }
        public string NatureID { get; set; }
        public string RelationshipCode { get; set; }
        public int AllowCreditTransaction { get; set; }
        public int AllowDebitTransaction { get; set; }
        public int NotServiceCharges { get; set; }
        public int NotStopPaymentCharges { get; set; }
        public int NotChequeBookCharges { get; set; }
        public string TurnOver { get; set; }
        public object NoOfDrTrx { get; set; }
        public object NoOfCrTrx { get; set; }
        public object DrThresholdLimit { get; set; }
        public object CrThresholdLimit { get; set; }
        public int ProductCash { get; set; }
        public int ProductClearing { get; set; }
        public int ProductCollection { get; set; }
        public int ProductRemittance { get; set; }
        public int ProductCross { get; set; }
        public int ProductOthers { get; set; }
        public string ProductOthersDesc { get; set; }
        public object NameKin { get; set; }
        public object FNameKin { get; set; }
        public object IDType { get; set; }
        public object IDNoKin { get; set; }
        public object AddressKin { get; set; }
        public object AddressKinCountryID { get; set; }
        public object AddressKinStateID { get; set; }
        public object AddressKinCityID { get; set; }
        public object PhNoResKin { get; set; }
        public object PhNoOffKin { get; set; }
        public object MobNoKin { get; set; }
        public object FaxKin { get; set; }
        public object EmailKin { get; set; }
        public object RelationshipKin { get; set; }
        public string SOCID { get; set; }
    }
}