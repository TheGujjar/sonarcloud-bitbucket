﻿namespace MWRetailOnboarding.Swagger.BankSmart.BalanceInquiry
{
    //REQUEST
    public class BalanceInquiryRequest : IExamplesProvider<BalanceInquiryModel>
    {
        public BalanceInquiryModel GetExamples()
        {
            return new BalanceInquiryModel()
            {
                AccountId = "0315100003348801",
                ClientId = "000033488"

            };
        }
    }

    //RESPONSE
    //public class BalanceInquiryResponse : IExamplesProvider<ActiveResponseSucces<Balance>>
    //{
    //    public ActiveResponseSucces<Balance> GetExamples()
    //    {
    //        return new ActiveResponseSucces<Balance>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new Balance()
    //            {
    //                AccountId = "0315100003348801"

    //            }

    //        };
    //    }
    //}
}

