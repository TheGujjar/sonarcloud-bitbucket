﻿namespace MWRetailOnboarding.Swagger.BankSmart.BalanceInquiry
{
    public class BalanceInquiryModel
    {
        public string AccountId { get; internal set; }
        public string ClientId { get; internal set; }
    }
}