﻿using MWRetailOnboarding.Swagger.BankSmart.BalanceInquiry;

namespace MWRetailOnboarding.Swagger.BankSmart.CloseAccount
{

    //REQUEST
    public class CloseAccountRequest : IExamplesProvider<BalanceInquiryModel>
    {
        public BalanceInquiryModel GetExamples()
        {
            return new BalanceInquiryModel()
            {
                AccountId = "0310100000014602"
            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}

}
