﻿using MWRetailOnboarding.Swagger.BankSmart.BalanceInquiry;
using System.Collections.Generic;

namespace MWRetailOnboarding.Swagger.BankSmart.CreateAccount
{
    //REQUEST
    public class CreateAccountRequest : IExamplesProvider<DBHandler.Model.Dtos.CreateAccount>
    {
        public DBHandler.Model.Dtos.CreateAccount GetExamples()
        {
            return new DBHandler.Model.Dtos.CreateAccount()
            {
                Acc = new Account()
                {
                    CreateBy = "1",
                    OurBranchID = "02",
                    ClientID = "000223239",
                    ProductID = "ZACCIN",
                    Name = "FAHAD ALI SYED SYE MAMOOD ALI",
                    Address = "Abu dhabi",
                    CountryID = "ARE",
                    StateID = "007",
                    CityID = "00016",
                    StatementFrequency = "M",
                    HoldMail = 0,
                    ZakatExemption = 0,
                    IntroducerAccountNo = "",
                    IntroducedBy = "",
                    IntroducerAddress = "",
                    IntroducerCityID = "",
                    IntroducerStateID = "",
                    IntroducerCountryID = "",
                    ModeOfOperation = "Singly",
                    Reminder = "",
                    Notes = "",
                    NatureID = "0302",
                    RelationshipCode = "RET",
                    AllowCreditTransaction = 0,
                    AllowDebitTransaction = 0,
                    NotServiceCharges = 1,
                    NotStopPaymentCharges = 1,
                    NotChequeBookCharges = 1,
                    TurnOver = "Below 1M",
                    NoOfDrTrx = null,
                    NoOfCrTrx = null,
                    DrThresholdLimit = null,
                    CrThresholdLimit = null,
                    ProductCash = 1,
                    ProductClearing = 1,
                    ProductCollection = 1,
                    ProductRemittance = 1,
                    ProductCross = 1,
                    ProductOthers = 1,
                    ProductOthersDesc = "1",
                    NameKin = null,
                    FNameKin = null,
                    IDType = null,
                    IDNoKin = null,
                    AddressKin = null,
                    AddressKinCountryID = null,
                    AddressKinStateID = null,
                    AddressKinCityID = null,
                    PhNoResKin = null,
                    PhNoOffKin = null,
                    MobNoKin = null,
                    FaxKin = null,
                    EmailKin = null,
                    RelationshipKin = null,
                    SOCID = "BASIC"
                },

                ClientIDS = new List<string>()
                {
                    "000223239"
                },
                SignatureInstructions = ""

            };
        }
    }

    //RESPONSE (not modified)
    //public class CIFExistsResponse : IExamplesProvider<ActiveResponseSucces<dynamic>>
    //{
    //    public ActiveResponseSucces<dynamic> GetExamples()
    //    {
    //        return new ActiveResponseSucces<dynamic>()
    //        {
    //            LogId = "",
    //            Status = new Status()
    //            {
    //                Code = "MSG-000000",
    //                Severity = "Success",
    //                StatusMessage = "Success"
    //            },
    //            Content = new dynamic()
    //            {


    //            }

    //        };
    //    }
    //}
}
