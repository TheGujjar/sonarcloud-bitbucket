﻿namespace MWRetailOnboarding.Swagger.BankSmart.GetInterestAccruedCurrentYear
{
    public class GetInterestAccruedCurrentYearDTO
    {
        public string AccountID { get; internal set; }
    }
}