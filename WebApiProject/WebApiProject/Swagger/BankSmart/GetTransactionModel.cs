﻿using System;

namespace MWRetailOnboarding.Swagger.BankSmart.GetTransactionListRetail
{
    public class GetTransactionModel
    {
        public DateTime StartDate { get; internal set; }
        public DateTime EndDate { get; internal set; }
        public string AccountID { get; internal set; }
    }
}